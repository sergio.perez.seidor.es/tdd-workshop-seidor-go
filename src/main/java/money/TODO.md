## REQUIREMENT

We’ll start with the object Ward created at WyCash, multi-currency money.

Suppose we have a report like this:

|Instrument |Shares |Price |Total|
|-----------|------:|------:|-----|
|IBM |1000| 25 |25000|
|GE |400 |100| 40000|
| | | |Total: 65000|

To make a multi-currency report, we need to add currencies:

|Instrument |Shares |Price |Total|
|-----------|------:|------:|-----|
|IBM |1000| 25 |25000|
|GE |400 |100| 40000|
| | | |Total: 65000|

## TODO

- [ ] 10 USD + 10 USD = 20 USD
- [ ] 15 USD + 15 USD = 30 USD
- [ ] Support conversion 1:1.05 eurs
- [ ] 10 USD + 10 EUR = 20.48 USD